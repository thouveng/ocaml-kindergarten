# ocaml_kindergarten

Keep track of activities around OCaml. It is not a project.

We are following this [tutorial](https://fr.wikibooks.org/wiki/Objective_Caml)
and also using the [Real World OCaml](https://realworldocaml.org/) book.
